UniConvertor is a universal vector graphics translator. It is a command line tool/library which uses SK2 document object model to convert one file format to another. The project is a multiplatform software and can be compiled under GNU/Linux (and other UNIX-like systems), macOS and Win32/64 operation systems.

We hope our projects and their derivatives will be helpful for you.

Load and Save
-------------

SK1 Editor (*.sk1)
AutoCAD Plot Output (*.plt)
Windows Metafile (*.wmf)

Read Only
---------

Adobe Illustrator (*.ai)
Corel Draw (*.ccx, *.cdr, *.cdt)
Computer Graphics Metafile (*.cgm)

